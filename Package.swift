// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 4.7.5
let package = Package(
    name: "FreestarAds-TAM",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-TAM",
            targets: ["FreestarAds-TAM", "FreestarAds-TAM-Core", "FMDBWrapper", "DTBiOSSDK"]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.22.0"
            ),
        .package(
            url: "https://github.com/maddapper/fmdb.git",
            exact: "2.7.6"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "FMDBWrapper",
            dependencies: [
                    .product(name: "FMDB", package: "FMDB")
                ]
            ),
        .binaryTarget(
            name: "FreestarAds-TAM",
            url: "https://gitlab.com/freestar/spm-freestarads-tam/-/raw/4.7.5/FreestarAds-TAM.xcframework.zip",
            checksum: "37922e9d54d823f25fe7bd6d4810d14cd161f8ef89e0d437207d0f99c41ceab0"
            ),
        .target(
            name: "FreestarAds-TAM-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .binaryTarget(
            name: "DTBiOSSDK",
            url: "https://gitlab.com/freestar/spm-freestarads-tam/-/raw/4.7.5/DTBiOSSDK.xcframework.zip",
            checksum: "2463420bbba68513e51771302a58474927876707d424e81563c2d7664738e522"
            )
    ]
)
